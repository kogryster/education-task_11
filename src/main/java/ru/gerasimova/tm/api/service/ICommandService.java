package ru.gerasimova.tm.api.service;

import ru.gerasimova.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArguments();

}
