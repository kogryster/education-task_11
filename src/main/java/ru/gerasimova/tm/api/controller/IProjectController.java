package ru.gerasimova.tm.api.controller;

public interface IProjectController {
    void showProjects();

    void clearProjects();

    void createProject();
}
