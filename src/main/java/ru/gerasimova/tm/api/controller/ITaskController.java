package ru.gerasimova.tm.api.controller;

public interface ITaskController {
    void showTasks();

    void clearTasks();

    void createTask();
}
